[
    {
        coor:{
            lat:1.286920,
            lng:103.854570
        },
        name : 'merlion',
        image :  require('../images/merlion.png'),
        place:'<div style="width:160px;height:20px;padding:0;float:left">Merlion</div>',
        desc : "The Merlion is the national personification of Singapore. "+
        'Its name combines "mer" meaning the sea and "lion". The ' +
        "fish body represents Singapore's origin as a fishing village " +
        'when it was called Temasek, which means "sea town" in Javanese. ' +
        " The lion head represents Singapore's original name—Singapura—meaning "+
        '"lion city" or "kota singa".' ,
         web :  'http://marinabaysands.com',
         address : '10 Bayfront avenue, Singapore'
        },
    {
        coor:{
            lat:1.287466,
            lng:103.851424
        },
        name : 'Garden by the Bay',
        place:'<div style="width:160px;height:20px;padding:0;float:left">Garden by the Bay</div>'

    },
    {
        coor:{
            lat:1.284193,
            lng:103.843362
        },
        name : 'Chinatown',
        place:'<div style="width:160px;height:20px;padding:0;float:left">Chinatown</div>'


    },
    {
        coor:{
            lat:1.281790,
            lng:103.863954
        },
        name : 'Garden bay sands',
        place:'<div style="width:160px;height:20px;padding:0;float:left">Garden bay sands</div>'

    },
    {
        coor:{
            lat:1.283099,
            lng:103.860295
        },
        name : 'Marina bay sands',
        place:'<div style="width:160px;height:20px;padding:0;float:left">Marina bay sands</div>'
    },
    {
        coor:{
            lat:1.289332,
            lng:103.863152
        },
        name : 'Singapore flyer',
        place:'<div style="width:160px;height:20px;padding:0;float:left">Singapore flyer</div>'
    },
    {
        coor:{
            lat:1.302279,
            lng:103.837399
        },
        name : 'Orchard Road',
        place:'<div style="width:160px;height:20px;padding:0;float:left">Orchard road</div>'
    },
    {
        coor:{
            lat:1.295526,
            lng:103.845331
        },
        name : 'Fort Canning Park',
        place:'<div style="width:160px;height:20px;padding:0;float:left">Fort Canningpark</div>'
    },
    {
        coor:{
            lat:1.290555,
            lng:103.846188
        },
        name : 'Clarke Quay',
        place:'<div style="width:160px;height:20px;padding:0;float:left">Clarke Quay</div>'
    },
    {
        coor:{
            lat:1.287466,
            lng:103.851424
        },
        name : 'Asian Civilisations Museum',
        place:'<div clss="popupgmaps">Asian Civilisations Museum</div>'

    },
    ]