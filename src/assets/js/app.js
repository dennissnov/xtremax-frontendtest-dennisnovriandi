require('bootstrap-loader');
const css = require('../scss/app.scss');
const json = require('../data/data.js');
console.log("dennissnov");
var $script = require("scriptjs");
var gmapslink = 'https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyD603VSNtl5cyfTWnyhCGTl0WjwULRNa2c&center=-33.9,151.14999999999998&zoom=12&format=png&maptype=roadmap&style=element:geometry%7Ccolor:0xf5f5f5&style=element:labels.icon%7Cvisibility:off&style=element:labels.text.fill%7Ccolor:0x616161&style=element:labels.text.stroke%7Ccolor:0xf5f5f5&style=feature:administrative.land_parcel%7Celement:labels.text.fill%7Ccolor:0xbdbdbd&style=feature:poi%7Celement:geometry%7Ccolor:0xeeeeee&style=feature:poi%7Celement:labels.text.fill%7Ccolor:0x757575&style=feature:poi.park%7Celement:geometry%7Ccolor:0xe5e5e5&style=feature:poi.park%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&style=feature:road%7Celement:geometry%7Ccolor:0xffffff&style=feature:road.arterial%7Celement:labels.text.fill%7Ccolor:0x757575&style=feature:road.highway%7Celement:geometry%7Ccolor:0xdadada&style=feature:road.highway%7Celement:labels.text.fill%7Ccolor:0x616161&style=feature:road.local%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&style=feature:transit.line%7Celement:geometry%7Ccolor:0xe5e5e5&style=feature:transit.station%7Celement:geometry%7Ccolor:0xeeeeee&style=feature:water%7Celement:geometry%7Ccolor:0xc9c9c9&style=feature:water%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&size=480x360'


$(document).ready(function(){
    var s = false;
    $(".sidebar-div").click(function() {
        $(this).siblings('.sidebar-div').removeClass('active');
        $(this).addClass('active');
        if(!s){
            $("#Judul").css('margin-left', '550px');
            $("#Sidenav").css('width', '350px');
            $("#main-container").css('margin-left', '500px');
            s = true;
        } else {
            $("#Judul").css('margin-left', '200px');
            $("#Sidenav").css('width', '0px');
            $("#main-container").css('margin-left', '150px');
            s = false;
        }
    })
})




//DROPDOWN
/**
 * 
var dropdown = document.getElementsByClassName("dropdown-list");
var i;
 * for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("sidenav-active");
    var dropCont = this.nextElementSibling;
    if (dropCont.style.display === "block") {
      dropCont.style.display = "none";
    } else {
      dropCont.style.display = "block";
    }
  });
}
 */
//DROP DOWN SIDENAV
$(document).ready(function(){
    $('.dropdown-list').click(function() {
        $(this).siblings('button').removeClass('sidenav-active');
        $(this).addClass('sidenav-active');
        if ($(this).next().css('display') === "block") {
            $(this).next().css('display', "none");         
        } else {
            console.log('ada')
            $(this).next().css('display', 'block');            

        }
    })
})

$(".close-button").click(function(){
    $("#Sideinfo").css('width', '0px');

})
//MAPS

// Google Maps
window.initMap = function initMap(){
    //Map options
    var options = {
      zoom:15,
      center:{lat:35.2271,lng:-80.8431}
    }
    // New map
    var map = new
    google.maps.Map(document.getElementById('googleMap'), options);
}

$script("https://maps.googleapis.com/maps/api/js?key=AIzaSyD603VSNtl5cyfTWnyhCGTl0WjwULRNa2c&callback=initMap", function initMap() {

        var options = {
        zoom:15,
        center:{lat:1.292069,lng:103.853819}
        }
        // New map
        var map = new
        google.maps.Map(document.getElementById('googleMap'), options);
        
        
        //MARKER
        const iconss = require('../images/marker.png');
        // DATA MARKERS
        var markers = [
            {
                coor:{
                    lat:1.286920,
                    lng:103.854570
                },
                name : 'merlion',
                image :  require('../images/merlion.png'),
                place:'<div style="width:160px;height:20px;padding:0;float:left">Merlion</div>',
                desc : "The Merlion is the national personification of Singapore. "+
                'Its name combines "mer" meaning the sea and "lion". The ' +
                "fish body represents Singapore's origin as a fishing village " +
                'when it was called Temasek, which means "sea town" in Javanese. ' +
                " The lion head represents Singapore's original name—Singapura—meaning "+
                '"lion city" or "kota singa".' ,
                 web :  'http://marinabaysands.com',
                 address : '10 Bayfront avenue, Singapore'
                },
            {
                coor:{
                    lat:1.287466,
                    lng:103.851424
                },
                name : 'Garden by the Bay',
                place:'<div style="width:160px;height:20px;padding:0;float:left">Garden by the Bay</div>'

            },
            {
                coor:{
                    lat:1.284193,
                    lng:103.843362
                },
                name : 'Chinatown',
                place:'<div style="width:160px;height:20px;padding:0;float:left">Chinatown</div>'


            },
            {
                coor:{
                    lat:1.281790,
                    lng:103.863954
                },
                name : 'Garden bay sands',
                place:'<div style="width:160px;height:20px;padding:0;float:left">Garden bay sands</div>'

            },
            {
                coor:{
                    lat:1.283099,
                    lng:103.860295
                },
                name : 'Marina bay sands',
                place:'<div style="width:160px;height:20px;padding:0;float:left">Marina bay sands</div>'
            },
            {
                coor:{
                    lat:1.289332,
                    lng:103.863152
                },
                name : 'Singapore flyer',
                place:'<div style="width:160px;height:20px;padding:0;float:left">Singapore flyer</div>'
            },
            {
                coor:{
                    lat:1.302279,
                    lng:103.837399
                },
                name : 'Orchard Road',
                place:'<div style="width:160px;height:20px;padding:0;float:left">Orchard road</div>'
            },
            {
                coor:{
                    lat:1.295526,
                    lng:103.845331
                },
                name : 'Fort Canning Park',
                place:'<div style="width:160px;height:20px;padding:0;float:left">Fort Canningpark</div>'
            },
            {
                coor:{
                    lat:1.290555,
                    lng:103.846188
                },
                name : 'Clarke Quay',
                place:'<div style="width:160px;height:20px;padding:0;float:left">Clarke Quay</div>'
            },
            {
                coor:{
                    lat:1.287466,
                    lng:103.851424
                },
                name : 'Asian Civilisations Museum',
                place:'<div clss="popupgmaps">Asian Civilisations Museum</div>'

            },
            ];
        var icon = {
            url: require('../images/marker.png'), // url

        };
        
        for(var i = 0;i<markers.length;i++){
            // Add marker
            addMarker(markers[i]);
        }
        function addMarker(props){
            var marker = new
              google.maps.Marker({
              position:props.coor,
              map:map,
              content:props.place,
              maxWidth: 120,
              pixelOffset :500,
              icon:iconss,
              desc : props.desc

            });
    
            var infoWindow = new google.maps.InfoWindow({
                content:props.place,
            });
            //  marker.setIcon(props.iconImage);


                        
            //SIDEINFOmain-container
  

    

            $(document).ready(function(){
                $(".sidenav-button").click(function() {
                    for(var i = 0;i<markers.length;i++){
                        // Add marker
                        par = markers[i]['name'];
                        cont = markers[i]['desc'];
                        console.log(par);
                        if(par == this.id) {
                            addr = markers[i]['address'];
                            web = markers[i]['web'];
                            if($("#Sideinfo").css('width') === "0px"){
                                $("#Sideinfo").css('width', '350px');
                                $("#Info-judul").html(par);
                                $("#Info-desc").html(cont);
                                $("#Info-web").html(web);
                                $("#Info-address").html(addr);
                            } else {
                                $("#Sideinfo").css('width', '0px');
                            }
                            break;
                        } else {

                        }
                    }
    
        
                })
                })
            infoWindow.open(map, marker);
   
            marker.addListener('click', function(){
                $("#Sideinfo").css('width', '350px');
                document.getElementById("Info-judul").innerHTML = marker['content'];
                document.getElementById("Info-desc").innerHTML = marker['desc'];
                document.getElementById("Info-address").innerHTML = marker['address'];
                document.getElementById("Info-web").innerHTML = marker['web'];

         });

          }
});

